#!/usr/bin/env python3
# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, Army, armymap, diplomacy_compare

# -----------
# Diplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----
    # citymap = {"Madrid": ['A']}  # , "London":['B']}
    # armymap = {"A": Army("A", "Madrid")}  # , "B":Army("B", "London")}

    def test_read(self):
        s = "A Madrid"
        i = diplomacy_read(s)
        # self.assertIsInstance(list,type(i))
        self.assertEqual(i[0],  "A")
        self.assertEqual(i[1], "Madrid")

    # ----
    # eval
    # ----

    def test_eval_hold(self):
        # VANILLA HOLD
        i = {}
        j = {}
        a = diplomacy_eval(["ABCDE", "Madrid", "Hold"], i, j)
        self.assertEqual(len(a), 2)
        self.assertEqual(i, {"Madrid": ["ABCDE"]})  # {"City":["A", "B"]}
        val1 = j["ABCDE"].name
        self.assertEqual(val1, "ABCDE")  # {"Army": [Army]}
        # armymap.pop('ABCDE')

    def test_eval_move(self):
        # VANILLA MOVE
        i = {}
        j = {}
        a = diplomacy_eval(["ABCDE", "Madrid", "Move", "Barcelona"], i, j)
        self.assertEqual(len(a), 2)
        # {"City":["A", "B"]}
        self.assertEqual(i, {"Madrid": [], "Barcelona": ["ABCDE"]})
        city1 = j["ABCDE"].city
        name1 = j["ABCDE"].name
        self.assertEqual(city1, "Barcelona")  # {"Army": [Army]}
        self.assertEqual(name1, "ABCDE")

    def test_eval_support(self):
        # VANILLA SUPPORT
        i = {}
        j = {}
        a = diplomacy_eval(["ABCDE", "Madrid", "Support", "FGHIJ"], i, j)
        self.assertEqual(i, {"Madrid": ["ABCDE"]})
        city1 = j["ABCDE"].city
        name1 = j["ABCDE"].name
        self.assertEqual(city1, "Madrid")
        # self.assertEqual(name1, "ABCDE")

        self.assertEqual(len(j), 2)
        city2 = j["FGHIJ"].city
        name2 = j["FGHIJ"].name
        stat2 = j["FGHIJ"].status
        self.assertEqual(city2, None)
        self.assertEqual(name2, "FGHIJ")
        self.assertEqual(stat2, 1)

    # ----
    # compare
    # ----
    # ! write better tests here
    def test_compare(self):
        c = {"London": ["A", "B", 'C']}  # , "London": ["C"]}
        ar1 = Army("A", "London", 2)
        ar2 = Army("B", "London", 3)
        ar3 = Army("C", "London")
        a = {"A": ar1, "B": ar2, "C": ar3}
        x, y = diplomacy_compare(c, a)

        self.assertEqual(y["B"].city, "London")
        self.assertEqual(y["A"].city, None)
        self.assertEqual(y["C"].city, None)

        self.assertEqual(len(x), len(c))
        self.assertEqual(len(y), len(a))

    def test_compare_tie_first(self):
        c = {"London": ["A", "B", 'C']}  # , "London": ["C"]}
        ar1 = Army("A", "London", 3)
        ar2 = Army("B", "London", 2)
        ar3 = Army("C", "London", 1)
        a = {"A": ar1, "B": ar2, "C": ar3}
        x, y = diplomacy_compare(c, a)

        self.assertEqual(y["A"].city, 'London')
        self.assertEqual(y["B"].city, None)
        self.assertEqual(y["C"].city, None)

        self.assertEqual(len(x), len(c))
        self.assertEqual(len(y), len(a))

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()

        # diplomacy_print(w)
        # self.assertEqual(w.getvalue(), "A Madrid\n")

        c = {"London": ["A", "B", 'C']}  # , "London": ["C"]}
        ar1 = Army("A", "London", 2)
        ar2 = Army("B", "London", 3)
        ar3 = Army("C", "London")
        a = {"A": ar1, "B": ar2, "C": ar3}
        # diplomacy_print(c, a, w)
        # self.assertEqual(w.getvalue(), "A London\nB London\nC London\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB London Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB London\n")

    def test_solve_move(self):
        r = StringIO("A Madrid Move London\nB London Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [Dead]\nB [Dead]\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
