#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  'A')
        self.assertEqual(city, 'Madrid')
        self.assertEqual(action, ('Hold',))

    def test_read2(self):
        s = "B Barcelona Move Madrid\n"
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  'B')
        self.assertEqual(city, 'Barcelona')
        self.assertEqual(action, ('Move', 'Madrid'))

    def test_read3(self):
        s = "C London Support B\n"
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  'C')
        self.assertEqual(city, 'London')
        self.assertEqual(action, ('Support', 'B'))

    def test_read4(self):
        s = "D Austin Move London\n"
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  'D')
        self.assertEqual(city, 'Austin')
        self.assertEqual(action, ('Move', 'London'))

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, 'A', 'Madrid')
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, 'B', 'London')
        self.assertEqual(w.getvalue(), "B London\n")

    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, 'C', 'Austin')
        self.assertEqual(w.getvalue(), "C Austin\n")

    def test_print4(self):
        w = StringIO()
        diplomacy_print(w, 'D', '[dead]')
        self.assertEqual(w.getvalue(), "D [dead]\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF NewYork Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Austin\nF NewYork\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC London\n")

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support C\nF NewYork Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Madrid\nD Paris\nE Austin\nF NewYork\n")

    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
