# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "A")
        self.assertEqual(i[1],  "Madrid")
        self.assertEqual(i[2],  ["Hold",""])
        self.assertEqual(i[3],  "Madrid")

    def test_read_2(self):
        s = "B Barcelona Move Madrid"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "B")
        self.assertEqual(i[1],  "Barcelona")
        self.assertEqual(i[2],  ["Move","Madrid"])
        self.assertEqual(i[3],  "Barcelona")

    def test_read_3(self):
        s = "C Paris Support A"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "C")
        self.assertEqual(i[1],  "Paris")
        self.assertEqual(i[2],  ["Support","A"])
        self.assertEqual(i[3],  "Paris")

    def test_read_4(self):
        s = "D London Support D"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "D")
        self.assertEqual(i[1],  "London")
        self.assertEqual(i[2],  ["Support","D"])
        self.assertEqual(i[3],  "London")

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, [["A","[dead]"],["B","Madrid"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [["A","[dead]"],["B","[dead]"],["C","[dead]"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [["A","Barcelona"]])
        self.assertEqual(w.getvalue(), "A Barcelona\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC Paris Move Barcelona\nD London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC Barcelona\nD London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move Moscow\nB Barcelona Move Madrid\nC Paris Move Barcelona\nD London Move Paris\nE Moscow Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Moscow\nB Madrid\nC Barcelona\nD Paris\nE London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC Paris Move Madrid\nD London Support B\nE Rome Support C\nF Athens Support A\nG Berlin Support F\nH Prague Move Berlin\nI Oslo Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Madrid\nD London\nE Rome\nF Athens\nG [dead]\nH [dead]\nI Oslo\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\nC Paris Move Barcelona\nD London Move Madrid\nE Rome Move London\nF Athens Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE London\nF Paris\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()
